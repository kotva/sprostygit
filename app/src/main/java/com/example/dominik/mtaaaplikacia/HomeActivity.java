package com.example.dominik.mtaaaplikacia;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.JsonReader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.*;
import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private final String url = "https://api.backendless.com/v1/data/restaurants";
    private ListView mListView;
    private String names[];
    private String addresses[];
    private int nOfObjects;
    private String ids[];
    private String urls[];
    private String categories[];
    private String owners[];
    private String nUrl;
    private ImageView refreshButton;
    private ImageView settingsButton;
    private String loggedUser = "";
    private EditText searchET;
    ProgressDialog progressDialog;
    MyDBHelper dbHelper = new MyDBHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final TextView mTitle = (TextView)findViewById(R.id.homeTV);
      //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        loggedUser = getIntent().getStringExtra("userID");
        progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Updating");
        progressDialog.setCancelable(false);
        progressDialog.show();
        mTitle.setText(TextUtils.isEmpty(getIntent().getStringExtra("login")) ? "Home" : (getIntent().getStringExtra("login")));
        searchET = (EditText)findViewById(R.id.searchET);
        searchET.setHorizontallyScrolling(true);
        searchET.setScrollbarFadingEnabled(true);
        searchET.setMovementMethod(new ScrollingMovementMethod());
        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(searchET.getText())) {
                    startList();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Button searchButton = (Button)findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchFor(searchET.getText().toString());
            }
        });
        Button mAddButton = (Button) findViewById(R.id.addButton);
       // mAddButton.setClickable(true);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(getIntent().getStringExtra("login"))){
                Intent addIntent = new Intent(HomeActivity.this, AddActivity.class);
                addIntent.putExtra("userID", loggedUser);
                addIntent.putExtra("login",getIntent().getStringExtra("login"));
                    startActivity(addIntent);}
                else{
                    showErr();
                }
            }
        });
        settingsButton = (ImageView)findViewById(R.id.homeSettingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(getIntent().getStringExtra("login"))){
                    Intent accountIntent = new Intent(HomeActivity.this,MyAccActivity.class);
                    accountIntent.putExtra("userID",loggedUser);
                    accountIntent.putExtra("login", getIntent().getStringExtra("login"));
                    startActivity(accountIntent);
                } else{
                    showErr();
                }
            }
        });

        refreshButton = (ImageView)findViewById(R.id.homeRefreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nOfObjects = 0;
                nUrl = null;
                names = null;
                addresses = null;
                ids = null;
                urls = null;
                categories = null;
                startUpdatingData();
            }
        });
        startUpdatingData();
    }

    private void searchFor(String what) {
        progressDialog.setMessage("Searching");
        progressDialog.show();
        LinkedList<String> results1 = new LinkedList<String>();
        LinkedList<String> results2 = new LinkedList<String>();
        LinkedList<String> results3 = new LinkedList<String>();
        for (int i = 0; i < names.length; i++) {
            if (names[i].contains(what)) {
                results1.add(names[i]);
                results2.add(addresses[i]);
                results3.add(categories[i]);
            }
        }
        String[] data1 = new String[results1.size()];
        String[] data2 = new String[results2.size()];
        String[] data3 = new String[results3.size()];
        for (int i = 0; i < results1.size(); i++) {
            data1[i] = results1.get(i);
            data2[i] = results2.get(i);
            data3[i] = results3.get(i);
        }
        startFilteredList(data1, data2, data3);
    }

    private void showError(int err) {
        ErrDialog errFragment = ErrDialog.newInstance(err);
        errFragment.show(getFragmentManager(), "Error");
    }

    private void showErr(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
        // set dialog message
        alertDialogBuilder
                .setTitle("You have to be logged in")

                .setNeutralButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void startList() {
        if ((names == null) || (addresses == null) || (categories == null)) {
            startUpdatingData();
        } else {
            TwoLineArrayAdapter adapter = new TwoLineArrayAdapter(this, R.layout.list_row, names, addresses, categories);
            mListView = (ListView) findViewById(R.id.restaurantList);
            mListView.setAdapter(adapter);
            try {
                if (progressDialog.isShowing() && progressDialog != null) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                Log.d("Error", e.toString());
            }
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent detailIntent = new Intent(HomeActivity.this, DetailActivity.class);
                    System.out.println(position);
                    detailIntent.putExtra("itemID", ids[position]);
                    detailIntent.putExtra("userID", loggedUser);
                    startActivity(detailIntent);
                }
            });
        }
    }

    private void startFilteredList(final String[] namess, String[] addressess, String[] categoriess) {
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_row, R.id.firstLine, names);
        TwoLineArrayAdapter adapter = new TwoLineArrayAdapter(this, R.layout.list_row, namess, addressess, categoriess);
        mListView = (ListView)findViewById(R.id.restaurantList);
        mListView.setAdapter(adapter);
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog.setMessage("Updating");
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(HomeActivity.this, DetailActivity.class);
                int i = 0;
                for (String element : names) {
                    if (element.equals(namess[position])) {
                        detailIntent.putExtra("itemID", ids[i]);
                    }
                    i++;
                }
                detailIntent.putExtra("userID", loggedUser);
                startActivity(detailIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        System.out.println("Stlacil som back haha!!");
        if(!TextUtils.isEmpty(getIntent().getStringExtra("login"))) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
            // set dialog message
            alertDialogBuilder
                    .setTitle("Log out?")
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    HomeActivity.this.finish();
                                    Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
                                    startActivity(loginIntent);
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
        }
        else{
            HomeActivity.this.finish();
            Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
            startActivity(loginIntent);
        }
    }

    private void startUpdatingData() {
        progressDialog.show();
        JSONObject obj = new JSONObject();
        CustomJSONObjectRequest dataRequest = new CustomJSONObjectRequest(Request.Method.GET, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //System.out.println(response.toString());
                JSONParser jsonParser = new JSONParser();
                nOfObjects = jsonParser.getTotalObjects(response);
                nUrl = jsonParser.getNextPageUrl(response);
                names = jsonParser.getStringFromJson(response, "rst_name");
                addresses = jsonParser.getStringFromJson(response, "rst_address");
                ids = jsonParser.getStringFromJson(response, "objectId");
                urls = jsonParser.getStringFromJson(response, "rst_img_url");
                categories = jsonParser.getStringFromJson(response, "rst_cat");
                owners = jsonParser.getStringFromJson(response, "ownerId");
                if (names.length < nOfObjects) {
                    updateData();
                } else {
                    startList();
                }
                //System.out.println(nOfObjects);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                readDatabase();
                if (error instanceof NoConnectionError) {
                    showError(0);
                } else if (error.networkResponse != null){
                    showError(error.networkResponse.statusCode);
                } else {
                    showError(1);
                }
            }
        });
        MTAAApplication.getInstance().addToRequestQueue(dataRequest, "datarequest");
    }

    private void updateData() {
        final JSONObject obj = new JSONObject();
        CustomJSONObjectRequest dataRequest = new CustomJSONObjectRequest(Request.Method.GET, nUrl, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //System.out.println(response.toString());
                JSONParser jsonParser = new JSONParser();
                nUrl = jsonParser.getNextPageUrl(response);
                names = arrayConcat(names, jsonParser.getStringFromJson(response, "rst_name"));
                addresses = arrayConcat(addresses, jsonParser.getStringFromJson(response, "rst_address"));
                ids = arrayConcat(ids, jsonParser.getStringFromJson(response, "objectId"));
                urls = arrayConcat(urls, jsonParser.getStringFromJson(response, "rst_img_url"));
                categories = arrayConcat(categories, jsonParser.getStringFromJson(response, "rst_cat"));
                owners = arrayConcat(owners, jsonParser.getStringFromJson(response, "ownerId"));
                if (names.length < nOfObjects) {
                    updateData();
                } else {
                    fillDatabase(names, addresses, categories, ids, owners, urls);
                    startList();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                readDatabase();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (error instanceof NoConnectionError) {
                    showError(0);
                } else if(error.networkResponse != null) {
                    showError(error.networkResponse.statusCode);
                } else
                    showError(1);
            }
        });
        MTAAApplication.getInstance().addToRequestQueue(dataRequest, "datarequest");
    }

    private String[] arrayConcat(String[] a, String[] b) {
        int aLen = a.length;
        int bLen = b.length;
        String[] c =  new String[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    private SQLiteDatabase fillDatabase(String[] n, String[] a, String[] c, String[] id, String[] oid, String[] u) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(MyDBHelper.FeedEntry.TABLE_NAME, null, null);
        for (int i = 0; i < n.length; i++) {
            ContentValues values = new ContentValues();
            values.put(MyDBHelper.FeedEntry.COLUMN_NAME_RST_NAME, n[i]);
            values.put(MyDBHelper.FeedEntry.COLUMN_NAME_RST_ADDRESS, a[i]);
            values.put(MyDBHelper.FeedEntry.COLUMN_NAME_RST_IMG, u[i]);
            values.put(MyDBHelper.FeedEntry.COLUMN_NAME_RST_CAT, c[i]);
            values.put(MyDBHelper.FeedEntry.COLUMN_NAME_RST_UUID, id[i]);
            values.put(MyDBHelper.FeedEntry.COLUMN_NAME_RST_OWNER, oid[i]);
            System.out.println(values);
            db.insert(MyDBHelper.FeedEntry.TABLE_NAME, null, values);
        }
        return db;
    }

    private Cursor readDatabase() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {MyDBHelper.FeedEntry.COLUMN_NAME_RST_NAME, MyDBHelper.FeedEntry.COLUMN_NAME_RST_ADDRESS,
                MyDBHelper.FeedEntry.COLUMN_NAME_RST_CAT, MyDBHelper.FeedEntry.COLUMN_NAME_RST_IMG, MyDBHelper.FeedEntry.COLUMN_NAME_RST_OWNER,
                MyDBHelper.FeedEntry.COLUMN_NAME_RST_UUID};
        Cursor c = db.query(
                MyDBHelper.FeedEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();
        String s = c.getString(0);
        System.out.println(s);
        fillFromDatabase(c);
        return c;
    }

    private void fillFromDatabase (Cursor c) {
        c.moveToFirst();
        LinkedList<String> n = new LinkedList<String>();
        LinkedList<String> a = new LinkedList<String>();
        LinkedList<String> ca = new LinkedList<String>();
        LinkedList<String> id = new LinkedList<String>();
        LinkedList<String> img = new LinkedList<String>();
        LinkedList<String> owner = new LinkedList<String>();

        do {
            n.add(c.getString(0));
            a.add(c.getString(1));
            ca.add(c.getString(2));
            img.add(c.getString(3));
            id.add(c.getString(4));
            owner.add(c.getString(5));
        } while (c.moveToNext());
        names = new String[n.size()];
        addresses = new String[n.size()];
        categories = new String[n.size()];
        urls = new String[n.size()];
        ids = new String[n.size()];
        owners = new String[n.size()];
        for (int i = 0; i < n.size(); i++) {
            names[i] = n.get(i);
            addresses[i] = a.get(i);
            categories[i] = ca.get(i);
            urls[i] = img.get(i);
            owners[i] = id.get(i);
            ids[i] = owner.get(i);
        }
        startList();
    }
}
