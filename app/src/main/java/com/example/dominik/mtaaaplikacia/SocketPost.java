package com.example.dominik.mtaaaplikacia;

//import java.net.Socket;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
/**
 * Created by RISO on 10. 5. 2016.
 */
public class SocketPost {
    public SocketPost(){

    Socket socket = null;

    IO.Options opts = new IO.Options();

    opts.secure = false;
    opts.port = 1341;
    opts.reconnection = true;
    opts.forceNew = true;
    opts.timeout = 5000;

    try{
        socket = IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts);//58a450be-854f-4bce-b7b4-b416ace16eb0
        //socket = IO.socket("http://sandbox.touch4it.com:1341/data/test/58a450be-854f-4bce-b7b4-b416ace16eb0");
        socket.connect();
    }catch(URISyntaxException e){
        e.printStackTrace();
    }

    JSONObject js = new JSONObject();
    try {
        js.put("url", "/data/charlie");
        js.put("user", "MB");
        js.put("data", new JSONObject().put("data", js
        ));
    }catch(Exception e) {
        e.printStackTrace();
    }

    socket.emit("post", js, new Ack() {
        @Override
        public void call(Object... args) {
            String text = null;
            ArrayList<String> reads = new ArrayList<String>();
            try {
                text = Arrays.toString(args);       //server odpoved
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!text.equals("")) {
                reads.add(text);
               // Log.d("Debug","put response: "+ reads.toString());
            }
        }
    });

    }
}
