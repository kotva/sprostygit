package com.example.dominik.mtaaaplikacia;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

/**
 * Created by Dominik on 17. 4. 2016.
 */
public class MyProgressDialog extends ProgressDialog {

    String msg;
    public MyProgressDialog(Context context, String message) {
        super(context);
        this.msg = message;
    }

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProgressDialog.Builder builder = new ProgressDialog.Builder(getContext());
        builder.setMessage(msg)
                .setCancelable(false)
                .setTitle("Please wait");

        // Create the AlertDialog object and return it
    }

}
