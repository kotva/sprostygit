package com.example.dominik.mtaaaplikacia;

/**
 * Created by Dominik on 17. 4. 2016.
 */
public class MyProperties {
    private static MyProperties mInstance= null;

    public String lastLogin = "";

    protected MyProperties(){}

    public static synchronized MyProperties getInstance(){
        if(null == mInstance){
            mInstance = new MyProperties();
        }
        return mInstance;
    }
}